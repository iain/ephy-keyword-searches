// bookmark shortcuts v1.1.1
// shortcuts for smart bookmark searches, shared with Deskbar Applet

// Original extension for the Mozilla-based Epiphany:
// (c) 2006-10-17 by Nigel Tao <nigeltao@gnome.org>, GPL licensed.
// Port to Seed and GIO, and add an entry to the Edit menu:
// Copyright © 2009 Iain Nicol <iainn@src.gnome.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

Gio = imports.gi.Gio;
Gdk = imports.gi.Gdk;

var shortcut_to_bookmark_map = {};
var shortcuts_file_name = GLib.get_user_config_dir() +
    "/deskbar-applet/search-bookmarks-shortcuts.txt";
var shortcuts_file_monitor;

var get_map_from_file = function (file) {
    var map = {};

    try {
        var data_in_stream = new Gio.DataInputStream.c_new(file.read());

        while ((line = data_in_stream.read_line(null)) !== null) {
            line = line.trim();

            var sep_location = line.search(/ |\t/);
            if (sep_location > 0) {
                var url = line.substring(0, sep_location);
                var shortcut = line.substring(sep_location + 1).trim();

                map[shortcut] = url;
            }
        }
    } catch (e) {
        // Maybe the file doesn't exist, etc.  Ignore.
        printerr(e);
    }

    return map;
};

var shortcuts_changed_cb = function (file_monitor, file, other_file,
                                     event_type, user_data) {
    if (event_type === Gio.FileMonitorEvent.CHANGES_DONE_HINT) {
        shortcut_to_bookmark_map = get_map_from_file(file);
    }
};

var edit_bookmark_shortcuts_cb = function (action, window) {
    // Comment out the code to spawn programs on the correct screen
    // because as of the end of 2009, missing annotations in GDK will
    // cause a crash.
    //
    var screen = null; // window.get_screen();
    var deskbar_extn = Gio.file_new_for_path(
        "/usr/lib/deskbar-applet/modules-2.20-compatible/epiphany.py");

    if (deskbar_extn.query_exists()) {
        // launch Deskbar's editor
        var python_commands =
            "import sys\n" +
            "sys.path.insert(0," +
            "    '/usr/lib/deskbar-applet/modules-2.20-compatible')\n" +
            "import epiphany\n" +
            "e = epiphany.EpiphanySearchHandler()\n" +
            "e.initialize()\n" +
            "e.show_config(None)\n";

        // Gdk.spawn_on_screen(screen, null, ["python", "-c", python_commands],
        //                     null, GLib.SpawnFlags.SEARCH_PATH, null, null,
        //                     null);
        GLib.spawn_async(null, ["python", "-c", python_commands],
                        null, GLib.SpawnFlags.SEARCH_PATH, null, null,
                        null);
    } else {
        var dialog = new Gtk.MessageDialog({
            text: "The file format is a series of lines, each of the form:\n" +
                "\n" +
                "url shortcut\n" +
                "\n" +
                "Each “url” should contain “%s” in the position the search " +
                "terms should go."});
        dialog.add_button(Gtk.STOCK_CANCEL,
                          Gtk.ResponseType.CANCEL);
        dialog.add_button("Open Editor", Gtk.ResponseType.OK).grab_focus();

        var response = dialog.run();
        dialog.destroy();

        if (response !== Gtk.ResponseType.OK) {
            return;
        }

        var shortcuts_file = Gio.file_new_for_path(shortcuts_file_name);
        try {
            shortcuts_file.create();
        } catch (e) {
        }
        Gtk.show_uri(screen, shortcuts_file.get_uri(), Gdk.CURRENT_TIME);
    }
};

var resolve_address_cb = function (bookmarks, address, content) {
    address = address.trim();
    var n = address.indexOf(" ");
    var shortcut;
    var args;

    if (n === -1) {
        shortcut = address;
        args = "";
    } else {
        shortcut = address.substring(0, n);
        args = address.substring(n+1);
    }

    bookmark = shortcut_to_bookmark_map[shortcut];
    if (bookmark === undefined) {
        return null;
    } else {
        return bookmark.replace("%s", args);
    }
};

var initialize_extension = function () {
    bookmarks = Epiphany.EphyShell.get_default().get_bookmarks();
    bookmarks._bookmark_shortcuts_signal = bookmarks.signal.
        resolve_address.connect(resolve_address_cb);

    var file = Gio.file_new_for_path(shortcuts_file_name);
    try {
        shortcuts_file_monitor = file.monitor();
        shortcuts_file_monitor.signal.changed.connect(shortcuts_changed_cb);
        shortcut_to_bookmark_map = get_map_from_file(file);
    } catch (e) {
    }
};

var finalize_extension = function () {
    bookmarks = Epiphany.EphyShell.get_default().get_bookmarks();
    bookmarks.signal.disconnect(bookmarks._bookmark_shortcuts_signal);
    delete bookmarks._bookmark_shortcuts_signal;
    shortcuts_file_monitor = null;
};

var attached_window_count = 0;

var attach_window = function (window) {
    // No race condition below because this extension is always called
    // in the one GUI thread.  Hopefully.
    if (attached_window_count === 0) {
        initialize_extension();
    }
    attached_window_count++;

    // Add Edit -> Bookmark Shortcuts to this window's menu.

    // Object to hold onto the Edit menu entry, etc., for the window's
    // lifetime.
    window._bookmark_shortcuts = {};
    var wbs = window._bookmark_shortcuts;

    wbs.action = new Gtk.Action({
        name: "EditBookmarkShortcuts",
        label: "_Bookmark Shortcuts",
        tooltip: "Configure shortcuts for your bookmark searches",
        stock_id: null});
    wbs.sig_id = wbs.action.signal.activate.connect(edit_bookmark_shortcuts_cb,
                                                    window);
    wbs.action_group = new Gtk.ActionGroup({
        name: "EditBookmarkShortcutsActionGroup"});
    wbs.action_group.add_action(wbs.action);

    // Unfortunately the following line can trigger:
    //     Gtk-CRITICAL **: gtk_container_foreach: assertion
    //     `GTK_IS_CONTAINER (container)' failed
    //  when we close the window.  This isn't our fault, though.
    wbs.manager = window.get_ui_manager();

    wbs.manager.insert_action_group(wbs.action_group, 0);
    wbs.merge_id = wbs.manager.new_merge_id();
    wbs.manager.add_ui(wbs.merge_id, "/menubar/EditMenu/EditPrefsMenu",
                       "EditBookmarkShortcutsMenu", "EditBookmarkShortcuts",
                       Gtk.UIManagerItemType.MENUITEM, true);
};

var detach_window = function (window) {
    // Remove our Edit menu entry.
    // Is this necessary?  Well, it doesn't hurt /too/ much, at least.
    var wbs = window._bookmark_shortcuts;

    wbs.manager.remove_ui(wbs.merge_id);
    wbs.manager.remove_action_group(wbs.action_group);
    wbs.action_group.remove_action(wbs.action);
    wbs.action.signal.disconnect(wbs.sig_id);
    delete window._bookmark_shortcuts;

    // Cleanup the rest of the extension if necessary.
    attached_window_count--;
    if (attached_window_count === 0) {
        finalize_extension();
    }
};

extension = {
    attach_window: attach_window,
    detach_window: detach_window
};
